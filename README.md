# OpenML dataset: SPECTF

https://www.openml.org/d/1600

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Krzysztof J. Cios","Lukasz A.  
**Source**: [original](https://archive.ics.uci.edu/ml/datasets/SPECTF+Heart) - Date unknown  
**Please cite**:   

This is a corrected version of the previous data file in version 1, which contained a dataset (349 instances) incorrectly merged from the original training and test sets available on UCI (there are actually 2 test sets there, one of them is incorrect). 
This file fixes that problem by merging the training set with the correct test set, resulting 267 instances.

SPECTF heart data:

This is a merged version of the separate train and test set which are usually distributed. On OpenML this train-test split can be found as one of the possible tasks.

NOTE: See the SPECT heart data for binary data for the same classification task.

Sources: -- Original owners: Krzysztof J. Cios, Lukasz A. Kurgan University of Colorado at Denver, Denver, CO 80217, U.S.A. Krys.Cios@cudenver.edu Lucy S. Goodenday Medical College of Ohio, OH, U.S.A. -- Donors: Lukasz A.Kurgan, Krzysztof J. Cios -- Date: 10/01/01

Relevant Information: The dataset describes diagnosing of cardiac Single Proton Emission Computed Tomography (SPECT) images. Each of the patients is classified into two categories: normal and abnormal. The database of 267 SPECT image sets (patients) was processed to extract features that summarize the original SPECT images. As a result, 44 continuous feature pattern was created for each patient. The CLIP3 algorithm was used to generate classification rules from these patterns. The CLIP3 algorithm generated rules that were 77.0% accurate (as compared with cardiologists' diagnoses).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1600) of an [OpenML dataset](https://www.openml.org/d/1600). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1600/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1600/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1600/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

